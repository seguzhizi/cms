package com.paic.cms.demo.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/demo")
public class DemoController {

	Logger logger = Logger.getRootLogger().getLogger(getClass());

    @RequestMapping("/index")
    public ModelAndView index(@RequestParam(value="username",required=false) String username){
    	String ss = "ssss1111";
    	System.out.println(ss);
    	

        new Thread(new runDeadLock()).start();
        
        
    	ModelAndView mav = new ModelAndView();
    	mav.addObject("username",username);
    	mav.setViewName("business");
        return mav;
    }
    

    @RequestMapping("/index2")
    public void index2(HttpServletRequest request,HttpServletResponse response) throws IOException{
    	PrintWriter writer = response.getWriter();
    	String str = "ssss111222444422a---";
    	System.out.println(str);
    	writer.write(str);
    	writer.flush();
    	writer.close();
    }
    
	
	class runDeadLock implements Runnable {

		@Override
		public void run() {
			while(true){
				logger.info("开始runDeadLock======("+Thread.currentThread().getName()+")=====" + new SimpleDateFormat("yyyy-HH-MM HH:mm:ss").format(new Date()));
				try {
					Thread.currentThread().sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				logger.info("结束runDeadLock======("+Thread.currentThread().getName()+")=====" + new SimpleDateFormat("yyyy-HH-MM HH:mm:ss").format(new Date()));
			}
		}
	}
	
	
}