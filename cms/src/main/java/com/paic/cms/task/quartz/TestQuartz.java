package com.paic.cms.task.quartz;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestQuartz {
	public static Logger logger = LoggerFactory.getLogger(TestQuartz.class);

	public void TestMethod() {
		logger.info("Auto Execute TestMethod start! Date={}", new DateTime().toString("YYYY-MM-DD HH:mm:ss"));
		logger.info("**********测试跑批类************");
		logger.info("Auto Execute TestMethod end! Date={}", new DateTime().toString("YYYY-MM-DD HH:mm:ss"));
	}
}
