package com.paic.cms.task.manager;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.paic.cms.task.service.TaskService;

public class TaskRunnable implements Runnable{

	@Autowired
	private TaskService taskService;
	@Autowired
	private TaskContext taskContext;
	private TaskDTO taskDTO;
	
	public TaskRunnable (TaskDTO taskDTO){
		this.taskDTO = taskDTO;
	}
	
	@Override
	public void run() {
		dispathcherTaskService();
	}

	private void dispathcherTaskService() {
		Map<String,TaskService> taskServiceMap = TaskContext.taskServiceMap;
		for(Map.Entry<String, TaskService> entry : taskServiceMap.entrySet()){
			if(entry.getKey().equals(taskDTO.getTaskType())){
				taskService = entry.getValue();
			}
			break;
		}
		
		taskService.doService();
		
	}

}
