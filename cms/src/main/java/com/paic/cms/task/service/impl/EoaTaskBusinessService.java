package com.paic.cms.task.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.paic.cms.task.handler.TaskHandler;
import com.paic.cms.task.service.TaskService;

@Component("EOA_REWRITE")
public class EoaTaskBusinessService implements TaskService{

	@Autowired
	private TaskHandler taskHandler;
	
	@Override
	public void doService() {
		taskHandler.doHandle();
	}

}
