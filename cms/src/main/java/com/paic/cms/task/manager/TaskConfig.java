package com.paic.cms.task.manager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskConfig {

	public static final String TASK_JAVA = "1";
	public static final String TASK_MYSQL = "2";
	public static final String TASK_PROCEDURE = "3";
	
	public class TaskServiceType{
		public static final String EOA_REWRITE 	=	"1";
		public static final String ALERM_EMAIL	=	"2";
		public static final String SCHEDULE_FIX	=	"3";
	}
	
	public static Map<String,List<String>> TaskRelationMap = new HashMap<String,List<String>>();
	static{
//		TaskRelationMap.put("", "");
	}

}
