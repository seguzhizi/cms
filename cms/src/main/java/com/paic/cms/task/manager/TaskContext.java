package com.paic.cms.task.manager;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.paic.cms.task.service.TaskService;

@Component
public class TaskContext implements ApplicationContextAware{

	Logger logger = Logger.getRootLogger().getLogger(getClass());
	
//	private TaskDTO taskDTO;
	public static ApplicationContext applicationContext;
	public static Map<String,TaskService> taskServiceMap = new HashMap<String,TaskService>();

//	public TaskContext(){
//	}
	
//	public TaskContext(TaskDTO taskDTO,Map<String,TaskService> taskServiceMap){
//		this.taskDTO = taskDTO;
//		this.taskServiceMap = taskServiceMap;
//	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
		String[] taskServiceNames = applicationContext.getBeanNamesForType(TaskService.class);
		logger.info(JSONObject.toJSONString(taskServiceNames));
		Map<String, TaskService> taskServiceMap = applicationContext.getBeansOfType(TaskService.class);
		logger.info(JSONObject.toJSONString(taskServiceMap));
	}
}
