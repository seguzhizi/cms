package com.paic.cms.task.handler;

import org.springframework.stereotype.Component;

@Component("integrationTaskHandler")
public class IntegrationTaskHandler implements TaskHandler{

	@Override
	public void doHandle() {
		System.out.println("-----------------IntegrationTaskHandler------------------");
	}

}
