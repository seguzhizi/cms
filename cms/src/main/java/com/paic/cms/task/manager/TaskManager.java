package com.paic.cms.task.manager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

public class TaskManager {

	Logger logger = Logger.getRootLogger().getLogger(getClass());
	
	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;
	//可从配置文件或者参数配置(DB)中读取
	private static int executeTaskNum = 5;	
	
	/**
	 * <p>1:定时任务开始执行</p>
	 * <p>2:获取所有处理任务的service bean，并按类型分类</p>
	 * <p>3:通过不同类型的bean去数据库中获取对应的任务，符合条件的就执行</p>
	 * 4:
	 */
	public void doManager(){
		logger.info("开始执行任务(TaskManager#doManager):" + new SimpleDateFormat("yyyy-HH-MM HH:mm:ss").format(new Date()));
//		List<TaskDTO> taskDTOList = getTaskDTOFromDB();
//		if(!taskDTOList.isEmpty()){
//			for(int i = 0 ; (i < executeTaskNum && i < taskDTOList.size()) ; i++ ){
//				threadPoolTaskExecutor.execute(new TaskRunnable(taskDTOList.get(i)));
//			}
//		}
	}
	
	public List<TaskDTO> getTaskDTOFromDB(){
		List<TaskDTO> taskDTOList = new ArrayList<TaskDTO>();

		TaskDTO taskDTO = new TaskDTO();
		taskDTO.setTaskType("INTEGRATION");
		taskDTO.setTaskId("i001");
		taskDTOList.add(taskDTO);
		
		TaskDTO taskDTO2 = new TaskDTO();
		taskDTO2.setTaskType("ACTIVITY");
		taskDTO2.setTaskId("a002");
		taskDTOList.add(taskDTO2);
		
		logger.info("TaskManager#getTaskDTOFromDB():共获取到"+taskDTOList.size()+"条任务");
		return taskDTOList;
	}
	
	
	
	
}
