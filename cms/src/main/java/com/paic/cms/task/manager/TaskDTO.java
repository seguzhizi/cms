package com.paic.cms.task.manager;

import java.io.Serializable;

public class TaskDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String taskId;
	private String taskType;
	private String lockThreadId;
	

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getLockThreadId() {
		return lockThreadId;
	}

	public void setLockThreadId(String lockThreadId) {
		this.lockThreadId = lockThreadId;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

}
