package com.paic.cms.task.quartz;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.log4j.Logger;

public class MyJobA {

	Logger logger = Logger.getRootLogger().getLogger(getClass());
	
	static boolean flag = true;

	public static void main(String[] args) throws IOException, InterruptedException {
//		Properties prop = new Properties();
//		ClassLoader classLoader = MyJobA.class.getClassLoader();
//		prop.load(classLoader.getResourceAsStream("url.properties"));
//		System.out.println(prop);
		new MyJobA().work();
		
	}
	public void work() throws InterruptedException, IOException{
		logger.info("测试接口时间为 " + new SimpleDateFormat("yyyy-HH-MM HH:mm:ss").format(new Date()));
		
		Properties prop = new Properties();
		ClassLoader classLoader = MyJobA.class.getClassLoader();
		prop.load(classLoader.getResourceAsStream("properties/url.properties"));
		logger.info("获取到URL文件内容："+prop);
		
        HttpClientTest client = new HttpClientTest();
        
        for(Entry<Object, Object> entry : prop.entrySet()){
        	String status = client.get(entry.getValue()+"");
        	logger.info("url:" + entry.getKey() +"---返回状态为：" + status);
        	Thread.currentThread().sleep(2000);
        }

       // new Thread(new runJPS()).start();
        
     }
	
	class runJPS implements Runnable {

		@Override
		public void run() {
			logger.info("开始runJPS==================("+Thread.currentThread().getName()+")==================" + new SimpleDateFormat("yyyy-HH-MM HH:mm:ss").format(new Date()));
			try {
				Thread.currentThread().sleep(10000);
				flag = false;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			logger.info("结束runJPS==================("+Thread.currentThread().getName()+")==================" + new SimpleDateFormat("yyyy-HH-MM HH:mm:ss").format(new Date()));
		}
	}	
}
